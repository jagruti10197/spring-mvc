package com.example.demo;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

//import com.sun.istack.NotNull;

//import com.sun.istack.NotNull;

@Entity
@Table(name="Student")
public class Student {
	
	//@NotEmpty(message="id cant be empty")
	@Id
	//@NotEmpty(message="id cant be empty")
	
	private int id;
	
//	@Pattern(regexp="[^0-9]*")
	@Size(min=2, max=30)
	@NotEmpty(message="name can't be empty")
//	@Column(name="name")
	private String name;
	
	@NotEmpty(message="email can't be empty")
	@Email(message="Enter valid email")
	private String email;
	
	
	//@Min(18)
	//@Max(100)
	//@NotEmpty(message="name can't be empty")
	//private int age;
	
	//@Max(8)
	@NotEmpty
//	@Column(name="password")
	@Pattern(regexp="[a-zA-Z0-9]{8}")
	private String password;
	
//	public Student(String name, String password) {
		// TODO Auto-generated constructor stub
	//}

	public int getid() {
		return id;
	}
	
	public void setid(int id) {
		this.id=id;
	}
	
	public String getname() {
		return name;
	}
	
	public void setname(String name) {
		this.name=name;
	}
	public String getemail() {
		return email;
	}
	
	public void setemail(String email) {
		this.email=email;
	}
	//public int getAge() {
//	return age;
//	}
	
	//public void setAge(int age) {
//		this.age=age;
//	}
	//@Min(2)
//	@Max(8)

	
	public String getPassword() {
	      return password;
	   }
	   public void setPassword(String password) {
	      this.password = password;
	   }
	
	@Override
	public String toString() {
		return "Student [id=" +id+",name="+name+",email="+email+",password="+password+"]";
	}
	
	
}
